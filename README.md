# dc_bridge #
A bridge for Dynamic Distributional Clauses to the Robot Operating System.

### Prerequisites ###

* Install [DC](https://bitbucket.org/problog/dc_problog)
* Install [ROS](http://www.ros.org/)

### Run the example model ###

* Setup a catkin workspace:
```
mkdir -p ~/<workspace>/src
cd ~/<workspace>/src
catkin_init_workspace
```
* Clone the dc_bridge repository into the src directory and build it:
```
cd <workspace>/src
git clone https://<username>@bitbucket.org/dtai_ros_robotics/dc_bridge.git
cd ..
catkin_make --pkg dc_bridge
```
* Run the the ROS node `dc_bridge/scripts/dc_example_model.py` that uses the model described in `dc_bridge/models/dc_example_model.pl`. To do this, enter the following commands in three different terminals:
```
roscore
rosrun dc_bridge dc_bridge
rosrun dc_bridge dc_example_model.py
```
You should see the following output in the third terminal:
```
probability: 1.0
probability: 0.0
args_ground: [o(1), o(2), o(3)]
probabilities: [1.0, 1.0, 1.0]
```
