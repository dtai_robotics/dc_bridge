#include <stdlib.h>

#include "ros/ros.h"
#include "ros/package.h"

#include "dc_bridge/InitializeService.h"
#include "dc_bridge/ReconsultService.h"
#include "dc_bridge/StepService.h"
#include "dc_bridge/QueryService.h"
#include "dc_bridge/QueryListService.h"
#include "dc_bridge/PlanStepService.h"
#include "dc_bridge/InitializePlanService.h"

#include "dcpf.h"
#include "dc.h"

#ifndef DC_BRIDGE_H
#define DC_BRIDGE_H


class DCUtil{
	public:
		bool initialized;
	private:
		dcpf pf;

		ros::NodeHandle _nh;

  	   ros::ServiceServer initialize_service;
  	   ros::ServiceServer reconsult_service;
  	   ros::ServiceServer step_service;
  	   ros::ServiceServer query_service;
     	ros::ServiceServer querylist_service;
		ros::ServiceServer planstep_service;
		ros::ServiceServer initializeplan_service;



		bool initialize(dc_bridge::InitializeService::Request &req, dc_bridge::InitializeService::Response &res );
		bool reconsult(dc_bridge::ReconsultService::Request &req, dc_bridge::ReconsultService::Response &res );
		bool step(dc_bridge::StepService::Request &req, dc_bridge::StepService::Response &res );
		bool query(dc_bridge::QueryService::Request &req, dc_bridge::QueryService::Response &res );
		bool querylist(dc_bridge::QueryListService::Request &req, dc_bridge::QueryListService::Response &res );
		bool plan_step(dc_bridge::PlanStepService::Request &req, dc_bridge::PlanStepService::Response &res);
		bool initialize_plan(dc_bridge::InitializePlanService::Request &req, dc_bridge::InitializePlanService::Response &res);


	public:
		DCUtil(ros::NodeHandle n);

		~DCUtil();
};

#endif
