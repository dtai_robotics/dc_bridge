#include <iostream>
#include <exception>
#include <string>
#include <Yap/YapInterface.h>
#include <Yap/c_interface.h>
#include "dc.h"
#include <vector>

using namespace std;

#ifndef DCPF_H
#define DCPF_H

class initerrordcpf: public std::exception
{
	virtual const char* what() const throw()
	{
	return "Initialization error: file not found or particle initialization failed";
	}
};


class dcpf: public dc
{

	public:
		// dcpf(string file, int numParticles);
		dcpf();

		int n;

		bool initialize(string model, int n_samples);
		bool reconsult(string model);
		bool step(string actions,string observations,double delta);
		bool plan_step(string observations, bool use_abstraction, uint32_t nb_samples, uint32_t max_horizon, uint32_t used_horizon, string &best_action, float &total_reward, uint32_t &time, bool &stop);
		double querylist(string id,string query,vector<string> &ids,vector<double> &probs);
		double get_plotdata(vector<double> &xs, vector<double> &ys,vector<double> &zs, vector<string> &color);
		double query(string q);
		// double get_gamma_newT();
		int getnumparticles(){return n;};
//		bool exec(string q);
};
#endif
