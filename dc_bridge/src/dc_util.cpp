#include "dc_util.h"


DCUtil::DCUtil(ros::NodeHandle n): _nh(n) {

	this->initialize_service = _nh.advertiseService("initialize_service", &DCUtil::initialize, this);
	this->reconsult_service = _nh.advertiseService("reconsult_service", &DCUtil::reconsult, this);
	this->step_service = _nh.advertiseService("step_service", &DCUtil::step, this);
	this->query_service = _nh.advertiseService("query_service", &DCUtil::query, this);
	this->querylist_service = _nh.advertiseService("querylist_service", &DCUtil::querylist, this);
	this->planstep_service = _nh.advertiseService("planstep_service", &DCUtil::plan_step, this);
	this->initializeplan_service = _nh.advertiseService("initializeplan_service", &DCUtil::initialize_plan, this);
}

DCUtil::~DCUtil(){}


bool DCUtil::initialize(dc_bridge::InitializeService::Request &req, dc_bridge::InitializeService::Response &res){
	pf.initialize(req.model, req.n_samples);
	res.result = 1;
	return 1;
}

bool DCUtil::reconsult(dc_bridge::ReconsultService::Request &req, dc_bridge::ReconsultService::Response &res){
	pf.reconsult(req.model);
	res.result = 1;
	return 1;
}

bool DCUtil::step(dc_bridge::StepService::Request &req, dc_bridge::StepService::Response &res){
	pf.step("", req.observation_string, 1);
	res.result = 1;
	return 1;
}

bool DCUtil::query(dc_bridge::QueryService::Request &req, dc_bridge::QueryService::Response &res){
	res.probability = pf.query(req.query_string);
	return 1;
}

bool DCUtil::querylist(dc_bridge::QueryListService::Request &req, dc_bridge::QueryListService::Response &res){
	vector<string> args;
	vector<double> probabilities;
	pf.querylist(req.query_arg, req.query_string, args, probabilities);

	res.probabilities = probabilities;
	res.args_ground = args;

	return 1;
}



bool DCUtil::plan_step(dc_bridge::PlanStepService::Request &req, dc_bridge::PlanStepService::Response &resp) {
	string best_action;
	float total_reward;
	uint32_t time;
	bool stop;

	bool res = pf.plan_step(req.observations_string, req.use_abstraction, req.nb_samples, req.max_horizon, req.used_horizon, best_action, total_reward, time, stop);

	if (!res) {
		return 0;
	}

	resp.best_action = best_action;
	resp.total_reward = total_reward;
	resp.time = time;
	resp.stop = stop;

	return 1;
}

bool DCUtil::initialize_plan(dc_bridge::InitializePlanService::Request &req, dc_bridge::InitializePlanService::Response &resp) {
	resp.result = pf.exec("executedplan_start");
	return 1;
}


int main(int argc, char **argv)
{
  ros::init(argc, argv, "dc_bridge_node");
  ros::NodeHandle nh;
  DCUtil dcutil(nh);

  ros::spin();
  return 0;
}
