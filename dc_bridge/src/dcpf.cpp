#include "dcpf.h"
#include <cstdio>
#include <cstring>
#include <iostream>
#include <string>
#include <vector>
#include <typeinfo>

void do_putc(int c)
{
	printf("%c",c);
}
initerrordcpf exceptiondcpf;

// dcpf::dcpf(string file, int numParticles):n(numParticles), dc(file){
dcpf::dcpf(): dc(){
}

bool dcpf::initialize(string model, int n_samples){
	n = n_samples;
	exec("reconsult('"+model+"')");
	YAP_Term error;
	char temp[100];
	sprintf(temp,"init_particle(%d)",n_samples);
	int res=YAP_RunGoalOnce(YAP_ReadBuffer(temp,&error));
	if(res!=1)
		throw exceptiondcpf;
	return 1;
}


bool dcpf::reconsult(string model){
	exec("reconsult('"+model+"')");
}



bool dcpf::step(string actions,string observations,double delta=1.0){
	string stepstring="step_particle([" + actions + "],[" + observations + "]," + std::to_string(n) + "," + std::to_string(delta) +")";
	YAP_Term error;
	char command[10000];
	strcpy(command,stepstring.c_str());
	int res = YAP_RunGoalOnce(YAP_ReadBuffer(command,&error));
	return res;
}



double dcpf::query(string query){
	YAP_Term error;
	string goal="eval_query_particle(" + query + "," + std::to_string(n) + ",ProbabilityQuery)";
	YAP_Term tmp = YAP_ReadBuffer(goal.c_str(),&error);
	long safe_t = YAP_InitSlot(tmp); // have a safe pointer to term
	int res = YAP_RunGoalOnce(tmp);
	if (res==false)
		return -1;
	double prob=YAP_FloatOfTerm( YAP_ArgOfTerm(3,YAP_GetFromSlot(safe_t)) );
	YAP_RecoverSlots(1); // safe copy not needed anymore

	return prob;
}

double dcpf::querylist(string id,string query,vector<string> &ids,vector<double> &probs){

	YAP_Term error;
	string goal="eval_query_particle2("+id+"," + query + "," + std::to_string(n) + ",ProbabilityQuery)";
	YAP_Term tmp = YAP_ReadBuffer(goal.c_str(),&error);
	long safe_t = YAP_InitSlot(tmp); // have a safe pointer to term
	int res = YAP_RunGoalOnce(tmp);
	if (res==false)
		return -1;

	int dimension=0;
	YAP_Term head, list = YAP_ArgOfTerm(4,YAP_GetFromSlot(safe_t));
	while(YAP_IsPairTerm(list))
	{
		head = YAP_HeadOfTerm(list);
		YAP_Term p=YAP_ArgOfTerm(1,head);
		char buff[10000];
		YAP_WriteBuffer(YAP_ArgOfTerm(2,head),buff,100,0);
		ids.push_back(string(buff));
		double prob;
		if(YAP_IsIntTerm(p))
				prob = YAP_IntOfTerm(p);
			else if(YAP_IsFloatTerm(p))
				prob = YAP_FloatOfTerm(p);
			else
				return false;
		probs.push_back(prob);

		list = YAP_TailOfTerm(list);
		dimension++;
	}
	if(dimension==0)
		return false;
	YAP_RecoverSlots(1); // safe copy not needed anymore
	return 0;
}


double dcpf::get_plotdata(vector<double> &xs, vector<double> &ys,vector<double> &zs, vector<string> &colors){

	YAP_Term error;
	string goal="get_plotdata("+std::to_string(n)+",Plotdata)";
	YAP_Term tmp = YAP_ReadBuffer(goal.c_str(),&error);
	long safe_t = YAP_InitSlot(tmp); // have a safe pointer to term
	int res = YAP_RunGoalOnce(tmp);
	if (res==false)
		return -1;

	int dimension=0;
	YAP_Term head, list = YAP_ArgOfTerm(2,YAP_GetFromSlot(safe_t));
	while(YAP_IsPairTerm(list)){


		head = YAP_HeadOfTerm(list);
		YAP_Term pos_x = YAP_ArgOfTerm(1,head);
		YAP_Term pos_y = YAP_ArgOfTerm(2,head);
		YAP_Term pos_z = YAP_ArgOfTerm(3,head);

		double x;
		double y;
		double z;

		if (YAP_IsIntTerm(pos_x) && YAP_IsIntTerm(pos_y) && YAP_IsIntTerm(pos_z)){
			x = YAP_IntOfTerm(pos_x);
			y = YAP_IntOfTerm(pos_y);
			z = YAP_IntOfTerm(pos_z);
		}
		else if (YAP_IsFloatTerm(pos_x) && YAP_IsFloatTerm(pos_y) && YAP_IsFloatTerm(pos_z)){
			x = YAP_FloatOfTerm(pos_x);
			y = YAP_FloatOfTerm(pos_y);
			z = YAP_FloatOfTerm(pos_z);
		}
		else
			return false;

		xs.push_back(x);
		ys.push_back(y);
		zs.push_back(z);

		char buff[10000];
		YAP_WriteBuffer(YAP_ArgOfTerm(4,head),buff,100,0);
		colors.push_back(string(buff));

		list = YAP_TailOfTerm(list);
		dimension++;
	}
	if(dimension==0)
		return false;
	YAP_RecoverSlots(1); // safe copy not needed anymore
	return 0;
}

inline const string bool_to_string(bool b) {
	return b ? string("true") : string("false");
}

bool dcpf::plan_step(string observations, bool use_abstraction, uint32_t nb_samples, uint32_t max_horizon, uint32_t used_horizon, string &best_action, float &total_reward, uint32_t &time, bool &stop) {
	YAP_Term error;
	string goal = "executedplan_step(BestAction, "
		+ bool_to_string(use_abstraction) + ", "
		+ observations + ", "
		+ std::to_string(nb_samples) + ", "
		+ std::to_string(max_horizon)
		+ ", TotalReward, "
		+ "Time, "
		+ std::to_string(used_horizon)
		+ ", Stop)";

	std::cout << goal << std::endl;
	YAP_Term tmp = YAP_ReadBuffer(goal.c_str(), &error);
	long safe_t = YAP_InitSlot(tmp); // have a safe pointer to term
	int res = YAP_RunGoalOnce(tmp);

	if (res == -1) {
		return -1;
	}

	YAP_Int best_action_slot = YAP_NewSlots(1);
	YAP_Int total_reward_slot = YAP_NewSlots(1);
	YAP_Int time_slot = YAP_NewSlots(1);
	YAP_Int stop_slot = YAP_NewSlots(1);

	YAP_PutInSlot(best_action_slot, YAP_ArgOfTerm(1, YAP_GetFromSlot(safe_t)));
	YAP_PutInSlot(total_reward_slot, YAP_ArgOfTerm(6, YAP_GetFromSlot(safe_t)));
	YAP_PutInSlot(time_slot, YAP_ArgOfTerm(7, YAP_GetFromSlot(safe_t)));
	YAP_PutInSlot(stop_slot, YAP_ArgOfTerm(9, YAP_GetFromSlot(safe_t)));

	if (!YAP_IsIntTerm(YAP_GetFromSlot(stop_slot)) || !YAP_IsIntTerm(YAP_GetFromSlot(time_slot)) || !YAP_IsFloatTerm(YAP_GetFromSlot(total_reward_slot))) {
		return -1;
	}

	stop = YAP_IntOfTerm(YAP_GetFromSlot(stop_slot));
	time = YAP_IntOfTerm(YAP_GetFromSlot(time_slot));
	total_reward = YAP_FloatOfTerm(YAP_GetFromSlot(total_reward_slot));

	char buff[10000];
	YAP_WriteBuffer(YAP_GetFromSlot(best_action_slot), buff, 10000, YAP_WRITE_HANDLE_VARS);

	best_action = string(buff);

   YAP_RecoverSlots(4);

	return 1;
}

/*
// execute an arbitrary prolog goal (query)
bool dcpf::exec(string q)
{
	YAP_Term error;
	int res = YAP_RunGoalOnce(YAP_ReadBuffer(q.c_str(),&error));
	return res;
}
*/
