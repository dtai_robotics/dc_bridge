#!/usr/bin/env python
import os

import rospy
import rospkg
from dc_pybridge import DCUtil

class Example():

    def __init__(self, model_file, n_samples):
        self.util = DCUtil(model_file, n_samples)

if __name__ == "__main__":
    rospy.init_node("example_node")

    path = rospkg.RosPack().get_path('dc_bridge')
    model_file = os.path.join(path, 'models/dc_example_model.pl')

    example = Example(model_file, 200)

    q1=example.util.query("(current(p(o(1)))~=1)")
    example.util.step("")
    q2=example.util.query("(current(p(o(1)))~=1)")
    example.util.step("")
    ql = example.util.querylist("ID","current(p(ID))~=3")

    print(q1)
    print(q2)

    print(ql)
