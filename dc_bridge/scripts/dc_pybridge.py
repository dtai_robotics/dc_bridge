#!/usr/bin/env python

import rospy
import rospkg

from dc_bridge.srv import InitializeService, ReconsultService, StepService, QueryService, QueryListService, PlanStepService, InitializePlanService


class DCUtil(object):
    def __init__(self, model, n_samples):
        rospy.wait_for_service('initialize_service')
        s = rospy.ServiceProxy('initialize_service', InitializeService)
        s(model, n_samples)

    def reconsult(self, model):
        rospy.wait_for_service('reconsult_service')
        s = rospy.ServiceProxy('reconsult_service', ReconsultService)
        resp = s(model)
        return resp

    def step(self, step_string):
        rospy.wait_for_service('step_service')
        s = rospy.ServiceProxy('step_service', StepService)
        resp = s(step_string)

    def query(self, query_string):
        rospy.wait_for_service('query_service')
        s = rospy.ServiceProxy('query_service', QueryService)
        resp = s(query_string)
        return resp

    def querylist(self, query_arg, query_string):
        rospy.wait_for_service('querylist_service')
        s = rospy.ServiceProxy('querylist_service', QueryListService)
        resp = s(query_arg, query_string)
        return resp


class DCPlannerUtil(DCUtil):
    """
    DCPlannerUtil class to interface with DC and the HYPE planner.

    Calls the InitializePlan service in the constructor to initialize the planner.
    """
    def __init__(self, *args, **kwargs):
        super(self.__class__, self).__init__(*args, **kwargs)
        rospy.wait_for_service('initializeplan_service')
        rospy.ServiceProxy('initializeplan_service', InitializePlanService)()

    def plan_step(self, observations, use_abstraction, nb_samples, max_horizon, used_horizon):
        """
        Executes one step in the planner.

        Keyword arguments:
        observations -- a string representing a list of observations
        use_abstraction -- boolean indicating whether or not to use abstraction in the planner
        nb_samples -- integer determining the number of samples to use in the step
        max_horizon -- integer determining the maximum horizon
        used_horizon -- integer determining the used horizon

        Returns:
        response object with fields "stop" (boolean) and "best_action" (string)
        """
        rospy.wait_for_service('planstep_service')
        s = rospy.ServiceProxy('planstep_service', PlanStepService)
        resp = s(observations, use_abstraction, nb_samples, max_horizon, used_horizon)
        return resp
