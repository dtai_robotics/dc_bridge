cmake_minimum_required(VERSION 2.8.3)
project(dc_bridge)


# Check c++11 / c++0x
include(CheckCXXCompilerFlag)
CHECK_CXX_COMPILER_FLAG("-std=c++11" COMPILER_SUPPORTS_CXX11)
CHECK_CXX_COMPILER_FLAG("-std=c++0x" COMPILER_SUPPORTS_CXX0X)
if(COMPILER_SUPPORTS_CXX11)
    set(CMAKE_CXX_FLAGS "-std=c++11")
elseif(COMPILER_SUPPORTS_CXX0X)
    set(CMAKE_CXX_FLAGS "-std=c++0x")
else()
    message(FATAL_ERROR "The compiler ${CMAKE_CXX_COMPILER} has no C++11 support. Please use a different C++ compiler.")
endif()


catkin_python_setup()


## Find catkin macros and libraries
## if COMPONENTS list like find_package(catkin REQUIRED COMPONENTS xyz)
## is used, also find other catkin packages
find_package(catkin REQUIRED COMPONENTS
  roscpp
  rospy
  roslib
  std_msgs
  message_generation
  geometry_msgs
)

## Generate services in the 'srv' folder
 add_service_files(
   FILES
   InitializeService.srv
   ReconsultService.srv
   StepService.srv
   QueryService.srv
   QueryListService.srv
   PlanStepService.srv
   InitializePlanService.srv
  )

generate_messages(
  DEPENDENCIES
  std_msgs
)




###################################
## catkin specific configuration ##
###################################
catkin_package(
  INCLUDE_DIRS include
  LIBRARIES dc_bridge
  CATKIN_DEPENDS
        std_msgs
        roscpp
        rospy
        geometry_msgs
	      message_runtime
)
###########
## Build ##
###########

## Specify additional locations of header files
## Your package locations should be listed before other locations

include_directories(
  include
  ${catkin_INCLUDE_DIRS}
)



add_executable(dc_bridge src/dc.cpp src/dcpf.cpp src/dc_util.cpp)






target_link_libraries(dc_bridge Yap)
target_link_libraries(dc_bridge mysqlclient)
target_link_libraries(dc_bridge gmp)
target_link_libraries(dc_bridge readline)
target_link_libraries(dc_bridge ${catkin_LIBRARIES})
