%%% -*- Mode: Prolog; -*-
:- use_module(library(distributionalclause)).
:- use_module(library(dcpf)).


:- set_options(default).
:- set_inference(backward(lazy)).



p(o(1)):0 ~ val(1).
p(o(2)):0 ~ val(1).
p(o(3)):0 ~ val(1).

p(X):t+1 ~ val(2)<- p(X):t~=1.
p(X):t+1 ~ val(3)<- p(X):t~=_.
